default: command

build-container:
	docker build -t tm2-integration-core:latest ./docker/php
command:
	docker run -v `pwd`:/app -it --name tm2-integration-core --rm tm2-integration-core $(c)
composer-install:
	docker run -v `pwd`:/app -it --name tm2-integration-core --rm -e XDEBUG_MODE=off tm2-integration-core composer install -n
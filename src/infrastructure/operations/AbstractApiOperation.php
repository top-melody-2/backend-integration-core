<?php

declare(strict_types=1);

namespace backendIntegrationCore\infrastructure\operations;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use DateTimeImmutable;
use DateTimeInterface;

abstract class AbstractApiOperation
{

    private ?DateTimeInterface $executionStartDateTime = null;

    private ?DateTimeInterface $executionEndDateTime = null;

    final protected function collectExecTime(callable $callable): void
    {
        $this->executionStartDateTime = new DateTimeImmutable();

        try {
            call_user_func($callable);
        } finally {
            $this->executionEndDateTime = new DateTimeImmutable();
        }
    }

    final protected function getRequestLog(): RequestLogDTO
    {
        return new RequestLogDTO(
            $this->getRequestUrl(),
            $this->getResponseStatusCode(),
            $this->getRequestAsString(),
            $this->getResponseAsString(),
            $this->getRequestHeaders(),
            $this->getResponseHeaders(),
            $this->executionStartDateTime,
            $this->executionEndDateTime,
        );
    }

    abstract protected function getRequestUrl(): string;

    abstract protected function getRequestHeaders(): HeaderCollection;

    abstract protected function getRequestAsString(): ?string;

    abstract protected function getResponseStatusCode(): ?int;

    abstract protected function getResponseAsString(): ?string;

    abstract protected function getResponseHeaders(): HeaderCollection;
}
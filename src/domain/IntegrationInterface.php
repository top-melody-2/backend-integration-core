<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain;

use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\ChangeTrackStateResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetInfoResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackStreamResponseDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use Throwable;

interface IntegrationInterface
{
    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getInfo(): GetInfoResponseDTO;

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getPlaylists(IntegrationDTO $integrationDTO): GetPlaylistsResponseDTO;

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getNextTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO): GetTrackResponseDTO;

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getTrackStream(IntegrationDTO $integrationDTO, TrackDTO $trackDTO): GetTrackStreamResponseDTO;

    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function canLikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool;

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function likeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO;

    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function canDislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool;

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function dislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO;
}
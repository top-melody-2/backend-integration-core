<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\exceptions;

use backendIntegrationCore\domain\dto\RequestLogDTO;
use Throwable;

class NeedLogException extends IntegrationException
{
    /**
     * @param RequestLogDTO[] $requestLogs
     */
    public function __construct(string $message, readonly public array $requestLogDtoList)
    {
        parent::__construct($message);
    }
}
<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\exceptions;

use Exception;

class IntegrationException extends Exception {}
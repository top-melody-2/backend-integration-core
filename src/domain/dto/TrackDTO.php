<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto;

use backendIntegrationCore\domain\collections\ConfigCollection;

readonly class TrackDTO
{

    public function __construct(
        public string $name,
        public int $duration,
        public ConfigCollection $config,
    ) {}
}
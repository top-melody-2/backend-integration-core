<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto\responses;

use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendIntegrationCore\domain\dto\StreamInterface;

readonly class GetTrackStreamResponseDTO extends LoggableResponseDTO
{
    /**
     * @param RequestLogDTO[] $requestLogDtoList
     */
    public function __construct(
        public array $requestLogDtoList,
        public StreamInterface $stream,
    ) {}
}
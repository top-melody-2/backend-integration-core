<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto\responses;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;

readonly class GetPlaylistsResponseDTO extends LoggableResponseDTO
{

    /**
     * @param RequestLogDTO[] $requestLogDtoList
     * @param PlaylistDTO[] $playlistDtoList
     */
    public function __construct(
        public array $requestLogDtoList,
        public ConfigCollection $integrationConfig,
        public array $playlistDtoList,
    ) {}
}
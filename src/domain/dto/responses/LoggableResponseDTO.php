<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto\responses;

use backendIntegrationCore\domain\dto\RequestLogDTO;

abstract readonly class LoggableResponseDTO
{

    /**
     * @var RequestLogDTO[]
     */
    public array $requestLogDtoList;
}
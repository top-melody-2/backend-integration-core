<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto\responses;

use backendIntegrationCore\domain\dto\IntegrationDTO;

readonly class GetInfoResponseDTO
{
    public function __construct(
        public IntegrationDTO $integrationDTO
    ) {}
}
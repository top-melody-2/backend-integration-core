<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto\responses;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\RequestLogDTO;

readonly class ChangeTrackStateResponseDTO extends LoggableResponseDTO
{

    /**
     * @param RequestLogDTO[] $requestLogDtoList
     */
    public function __construct(
        public array $requestLogDtoList,
        public ConfigCollection $integrationConfig,
        public ConfigCollection $playlistConfig,
        public ConfigCollection $trackConfig,
    ) {}
}
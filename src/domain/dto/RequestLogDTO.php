<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto;

use backendIntegrationCore\domain\collections\HeaderCollection;
use DateTimeInterface;

readonly class RequestLogDTO
{

    public function __construct(
        public string               $requestUrl,
        public ?int                 $responseStatusCode,
        public ?string              $request,
        public ?string              $response,
        public HeaderCollection     $requestHeaders,
        public HeaderCollection     $responseHeaders,
        public DateTimeInterface    $executionStartDateTime,
        public DateTimeInterface    $executionEndDateTime,
    ) {}
}
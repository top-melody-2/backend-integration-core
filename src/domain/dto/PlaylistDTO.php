<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto;

use backendIntegrationCore\domain\collections\ConfigCollection;

readonly class PlaylistDTO
{

    public function __construct(
        public string $name,
        public ConfigCollection $config,
    ) {}
}
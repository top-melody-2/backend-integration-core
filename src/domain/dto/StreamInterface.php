<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto;

interface StreamInterface
{
    public function getStream(): callable;
    public function getName(): string;
    public function getExtension(): string;
    public function getSizeInBytes(): int;
}
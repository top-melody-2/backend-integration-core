<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\dto;

use backendIntegrationCore\domain\collections\HeaderCollection;

abstract class AbstractOperationDTO
{

    public RequestLogDTO $requestLog;
    public ?int $responseStatusCode;
    public HeaderCollection $responseHeaders;
}
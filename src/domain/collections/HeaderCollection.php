<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\collections;

use Illuminate\Support\Collection;

/**
 * @inheritDoc
 * @property array<string, string> $items
 */
class HeaderCollection extends Collection
{

    private const GUZZLE_SEPARATOR = '; ';

    /**
     * @param array<string, string[]> $guzzleArray
     * @return self
     */
    public static function fromGuzzleArray(array $guzzleArray): self
    {
        $items = [];
        foreach ($guzzleArray as $name => $values) {
            $items[$name] = implode(self::GUZZLE_SEPARATOR, $values);
        }

        return new self($items);
    }

    /**
     * @param string $key
     */
    public function has($key): bool
    {
        return parent::has($key);
    }

    /**
     * @param string $key
     * @param string|null $default
     */
    public function get($key, $default = null): ?string
    {
        return parent::get($key, $default);
    }

    /**
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return parent::toArray();
    }

    public function set(string $name, string $value): self
    {
        $this->items[$name] = $value;
        return $this;
    }
}
<?php

declare(strict_types=1);

namespace backendIntegrationCore\domain\collections;

use Illuminate\Support\Collection;

/**
 * @inheritDoc
 * @property array<string, mixed> $items
 */
class ConfigCollection extends Collection
{

    /**
     * @param string $key
     */
    public function has($key): bool
    {
        return parent::has($key);
    }

    /**
     * @param string $key
     * @param mixed $default
     */
    public function get($key, $default = null): mixed
    {
        return parent::get($key, $default);
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return parent::toArray();
    }

    public function set(string $name, mixed $value): self
    {
        $this->items[$name] = $value;
        return $this;
    }
}